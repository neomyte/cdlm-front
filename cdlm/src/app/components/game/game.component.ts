import { Component, OnInit } from '@angular/core';
import { ConceptService } from '../../services/concept.service';
import { Concept } from '../../models/concept.model';

@Component({
	selector: 'app-game',
	templateUrl: './game.component.html',
	styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
	concepts: Array<Concept>;

	constructor(private conceptService: ConceptService) { }

	ngOnInit() {
		this.conceptService.getAll().subscribe(data => {
			this.concepts = data.information.list;
		});
	}
}
