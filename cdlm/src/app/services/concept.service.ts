import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
	providedIn: 'root'
})
export class ConceptService {

	constructor(private http: HttpClient) { }

	getAll(): Observable<any> {
		//http://back:4200/read/concepts
		return this.http.get('http://zigip.hopto.org:999/read/concepts');
	}
}
