export class Concept{
    id: number;
    name: string;
    pure: boolean;
    dead: boolean;
    parents: Array<Concept>;
}