# CDLM-Front

# Description

This repo contains all the front code for the site called Concept De La Muerte.

# Tech stack

* **Angular 7**
* **Bootstrap**

# Contributors

* Romain Haas
* Emmanuel Pluot